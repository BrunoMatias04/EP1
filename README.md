# EP1 - OO (UnB - Gama)
* obs: Makefile deve contar a flag -syd=c++11.
Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

# Orientação a Objetos 2016

* Nome : Bruno Matias Casas
* Matricula : 150051212 

### Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o terminal;
* Encontre o diretório raiz do projeto;
* Limpe os arquivos objeto:
	**$ make clean** 
* Compile o programa: 
	**$ make**
* Execute:
	**$ make run**
### Ao executar o programa ...

* Escolher o filtro para ser aplicado na imagem , escolhendo seu numero referente , digitando-o no terminal e dando um [Enter]. 
* Digitar o local da imagem ppm desejada(Ex:/home/bmatias/oo2016/EP0/imagens/unb.ppm).
* Isira [Enter] para que o programa leia a imagem e printe sua largura,altura e a escala maxima das cores.
* Alem disso o programa irá salvar todo conteudo filtrado em um arquivo chamado "Arquivo_novo.ppm " na pasta do projeto.
