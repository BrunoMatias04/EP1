#include "Media.hpp"
Media::Media(){
}
Media::~Media(){
}
void Media::Filtrar(){
	string nome_nova_imagem = "Arquivo_Novo.ppm";
	ofstream Arquivo_Novo(nome_nova_imagem.c_str());
	Arquivo_Novo << getNumero_magico() << endl;
	Arquivo_Novo << getLargura() << endl;
	Arquivo_Novo << getAltura() << endl;
	Arquivo_Novo << getCor_Max() << endl;
	int aux = 0;
	Pixel *pxl = getP();
	Pixel **pxl2;
	pxl2 = (Pixel**)malloc(getAltura()*sizeof(Pixel*));
	for(int i = 0;i < getAltura();i++){
		pxl2[i] = (Pixel *)malloc(getLargura()*sizeof(Pixel));
	}	
	for(int aux1 = 0;aux1 < getAltura();aux1++){
		for(int aux2 = 0;aux2 < getLargura();aux2++){
			pxl2[aux1][aux2].setR(pxl[aux].getR());
			pxl2[aux1][aux2].setG(pxl[aux].getG());
			pxl2[aux1][aux2].setB(pxl[aux].getB());
			aux++;	
		}
	}
	aux = 0;
	Pixel value;
	for(int linha = 1;linha < getAltura()-2;linha++){
		for(int coluna = 1;coluna < getLargura()-2;coluna++){
			value.setR(0);
			value.setG(0);
			value.setB(0);
			for(int i =linha-1;i<=linha+1;i++){
				for(int j =coluna-1;j<=coluna+1;j++){
					value.setR(value.getR()+(1.0/9.0)*pxl2[i+1][j+1].getR());
					value.setG(value.getG()+(1.0/9.0)*pxl2[i+1][j+1].getG());
					value.setB(value.getB()+(1.0/9.0)*pxl2[i+1][j+1].getB());
				}
			}
		
		pxl2[linha][coluna].setR(value.getR());
		pxl2[linha][coluna].setG(value.getG());
		pxl2[linha][coluna].setB(value.getB());
		}
	}
	for(int aux5 = 0;aux5 < getAltura();aux5++){
		for(int aux6 = 0;aux6 < getLargura();aux6++){
			Arquivo_Novo << pxl2[aux5][aux6].getR();
			Arquivo_Novo << pxl2[aux5][aux6].getG();
			Arquivo_Novo << pxl2[aux5][aux6].getB();
		}	
	} 
	cout << "Filtrado e criado o arquivo ""Arquivo_Novo.ppm"" com a imagem filtrada na pasta do programa!!"<< endl;
	Arquivo_Novo.close();
}
void Media::Filtrar5x5(){
	string nome_nova_imagem = "Arquivo_Novo.ppm";
	ofstream Arquivo_Novo(nome_nova_imagem.c_str());
	Arquivo_Novo << getNumero_magico() << endl;
	Arquivo_Novo << getLargura() << endl;
	Arquivo_Novo << getAltura() << endl;
	Arquivo_Novo << getCor_Max() << endl;
	int aux = 0;
	int limite = 5/2;
	Pixel *pxl = getP();
	Pixel **pxl2;
	pxl2 = (Pixel**)malloc(getAltura()*sizeof(Pixel*));
	for(int i = 0;i < getAltura();i++){
		pxl2[i] = (Pixel *)malloc(getLargura()*sizeof(Pixel));
	}	
	for(int aux1 = 0;aux1 < getAltura();aux1++){
		for(int aux2 = 0;aux2 < getLargura();aux2++){
			pxl2[aux1][aux2].setR(pxl[aux].getR());
			pxl2[aux1][aux2].setG(pxl[aux].getG());
			pxl2[aux1][aux2].setB(pxl[aux].getB());
			aux++;	
		}
	}
	aux = 0;
	Pixel value;
	for(int linha = limite;linha < getAltura()-limite-1;linha++){
		for(int coluna = limite;coluna < getLargura()-limite-1;coluna++){
			value.setR(0);
			value.setG(0);
			value.setB(0);
			for(int i =linha-limite;i<=linha+limite;i++){
				for(int j =coluna-limite;j<=coluna+limite;j++){
					value.setR(value.getR()+(1.0/25.0)*pxl2[i+1][j+1].getR());
					value.setG(value.getG()+(1.0/25.0)*pxl2[i+1][j+1].getG());
					value.setB(value.getB()+(1.0/25.0)*pxl2[i+1][j+1].getB());
				}
			}
		
		pxl2[linha][coluna].setR(value.getR());
		pxl2[linha][coluna].setG(value.getG());
		pxl2[linha][coluna].setB(value.getB());
		}
	}
	for(int aux5 = 0;aux5 < getAltura();aux5++){
		for(int aux6 = 0;aux6 < getLargura();aux6++){
			Arquivo_Novo << pxl2[aux5][aux6].getR();
			Arquivo_Novo << pxl2[aux5][aux6].getG();
			Arquivo_Novo << pxl2[aux5][aux6].getB();
		}	
	} 
	cout << "Filtrado e criado o arquivo ""Arquivo_Novo.ppm"" com a imagem filtrada na pasta do programa!!"<< endl;
	Arquivo_Novo.close();
}
void Media::Filtrar7x7(){
	string nome_nova_imagem = "Arquivo_Novo.ppm";
	ofstream Arquivo_Novo(nome_nova_imagem.c_str());
	Arquivo_Novo << getNumero_magico() << endl;
	Arquivo_Novo << getLargura() << endl;
	Arquivo_Novo << getAltura() << endl;
	Arquivo_Novo << getCor_Max() << endl;
	int aux = 0;
	int limite = 7/2;
	Pixel *pxl = getP();
	Pixel **pxl2;
	pxl2 = (Pixel**)malloc(getAltura()*sizeof(Pixel*));
	for(int i = 0;i < getAltura();i++){
		pxl2[i] = (Pixel *)malloc(getLargura()*sizeof(Pixel));
	}	
	for(int aux1 = 0;aux1 < getAltura();aux1++){
		for(int aux2 = 0;aux2 < getLargura();aux2++){
			pxl2[aux1][aux2].setR(pxl[aux].getR());
			pxl2[aux1][aux2].setG(pxl[aux].getG());
			pxl2[aux1][aux2].setB(pxl[aux].getB());
			aux++;	
		}
	}
	aux = 0;
	Pixel value;
	for(int linha = limite;linha < getAltura()-limite-1;linha++){
		for(int coluna = limite;coluna < getLargura()-limite-1;coluna++){
			value.setR(0);
			value.setG(0);
			value.setB(0);
			for(int i =linha-limite;i<=linha+limite;i++){
				for(int j =coluna-limite;j<=coluna+limite;j++){
					value.setR(value.getR()+(1.0/49.0)*pxl2[i+1][j+1].getR());
					value.setG(value.getG()+(1.0/49.0)*pxl2[i+1][j+1].getG());
					value.setB(value.getB()+(1.0/49.0)*pxl2[i+1][j+1].getB());
				}
			}
		
		pxl2[linha][coluna].setR(value.getR());
		pxl2[linha][coluna].setG(value.getG());
		pxl2[linha][coluna].setB(value.getB());
		}
	}
	for(int aux5 = 0;aux5 < getAltura();aux5++){
		for(int aux6 = 0;aux6 < getLargura();aux6++){
			Arquivo_Novo << pxl2[aux5][aux6].getR();
			Arquivo_Novo << pxl2[aux5][aux6].getG();
			Arquivo_Novo << pxl2[aux5][aux6].getB();
		}	
	} 
	cout << "Filtrado e criado o arquivo ""Arquivo_Novo.ppm"" com a imagem filtrada na pasta do programa!!"<< endl;
	Arquivo_Novo.close();
}
