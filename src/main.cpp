#include <iostream>
#include "Imagem.hpp"
#include "Filtro.hpp"
#include "Polarizado.hpp"
#include "Preto_e_Branco.hpp"
#include "Media.hpp"
#include "Negativo.hpp"
int main(){
	int escolha;
	cout << "Escolha o numero do filtro desejado:\n1-Negativo\n2-Polarizado\n3-Preto e Branco\n4-Media 3x3\n5-Media 5x5\n6-Media 7x7\n";
	cout << "-> ";
	cin >> escolha;
	if(escolha == 1){
		Negativo *imagem = new Negativo();
		string nome_da_imagem,pix;
		cout << "Digite o local da Imagem: " << endl;
		cout << "-> ";
		cin >> nome_da_imagem;
		imagem->setNome_da_Imagem(nome_da_imagem);
		imagem->LerImagem();
		pix = imagem->getPixels();
		imagem->InserindoPixels(pix);
		imagem->Filtrar();
		delete(imagem);
	}
	else if(escolha == 2){
		Polarizado *imagem = new Polarizado();
		string nome_da_imagem,pix;
		cout << "Digite o local da Imagem: " << endl;
		cout << "-> ";
		cin >> nome_da_imagem;
		imagem->setNome_da_Imagem(nome_da_imagem);
		imagem->LerImagem();
		pix = imagem->getPixels();
		imagem->InserindoPixels(pix);
		imagem->Filtrar();
		delete(imagem);
	}
	else if(escolha == 3){
		Preto_e_Branco *imagem = new Preto_e_Branco();
		string nome_da_imagem,pix;
		cout << "Digite o local da Imagem: " << endl;
		cout << "-> ";
		cin >> nome_da_imagem;
		imagem->setNome_da_Imagem(nome_da_imagem);
		imagem->LerImagem();
		pix = imagem->getPixels();
		imagem->InserindoPixels(pix);
		imagem->Filtrar();
		delete(imagem);
	}
	else if(escolha == 4){
                Media *imagem = new Media();
                string nome_da_imagem,pix;
                cout << "Digite o local da Imagem: " << endl;
		cout << "-> ";
                cin >> nome_da_imagem;
                imagem->setNome_da_Imagem(nome_da_imagem);
                imagem->LerImagem();
                pix = imagem->getPixels();
                imagem->InserindoPixels(pix);
                imagem->Filtrar();
                delete(imagem);
        }
	else if(escolha == 5){
		Media *imagem = new Media();
                string nome_da_imagem,pix;
                cout << "Digite o local da Imagem: " << endl;
		cout << "-> ";
                cin >> nome_da_imagem;
                imagem->setNome_da_Imagem(nome_da_imagem);
                imagem->LerImagem();
                pix = imagem->getPixels();
                imagem->InserindoPixels(pix);
                imagem->Filtrar5x5();
                delete(imagem);
	}
	else if(escolha == 6){
		Media *imagem = new Media();
                string nome_da_imagem,pix;
                cout << "Digite o local da Imagem: " << endl;
		cout << "-> ";
                cin >> nome_da_imagem;
                imagem->setNome_da_Imagem(nome_da_imagem);
                imagem->LerImagem();
                pix = imagem->getPixels();
                imagem->InserindoPixels(pix);
                imagem->Filtrar7x7();
                delete(imagem);
	}
	return 0;
}
