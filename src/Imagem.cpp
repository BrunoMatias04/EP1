#include "Imagem.hpp"

using namespace std;

Imagem::Imagem(){
	setPixels(" ");
	setComentario(" ");
	setLargura(0);
	setAltura(0);
	setCor_Max(0);
	setNome_da_Imagem(" ");
	setNumero_magico(" ");
}

Imagem::Imagem(string Pixels,string Comentario,string Numero_magico,int Largura,int Altura,int Cor_Max,string Nome_da_Imagem){
	setPixels(Pixels);
	setComentario(Comentario);
	setLargura(Largura);
	setAltura(Altura);
	setCor_Max(Cor_Max);
	setNome_da_Imagem(Nome_da_Imagem);
	setNumero_magico(Numero_magico);
}
Imagem::~Imagem(){
}
string Imagem::getPixels()
{
	return Pixels;
}
string Imagem::getComentario()
{
	return Comentario;
}
int Imagem::getLargura()
{
	return Largura;
}
int Imagem::getAltura()
{
	return Altura;
}
int Imagem::getCor_Max()
{
	return Cor_Max;
}
string Imagem::getNome_da_Imagem()
{
	return Nome_da_Imagem;
}
string Imagem::getNumero_magico()
{
	return Numero_magico;
}
void Imagem::setPixels(string Pixels)
{
	this->Pixels = Pixels;
}
void Imagem::setComentario(string Comantario)
{
	this->Comentario = Comantario;
}
void Imagem::setLargura(int Largura)
{
	this->Largura = Largura;
}
void Imagem::setAltura(int Altura)
{
	this->Altura = Altura;
}
void Imagem::setCor_Max(int Cor_Max)
{
	this->Cor_Max = Cor_Max;
}
void Imagem::setNome_da_Imagem(string Nome_da_Imagem)
{
	this->Nome_da_Imagem = Nome_da_Imagem;
}
void Imagem::setNumero_magico(string Numero_magico)
{
	this->Numero_magico = Numero_magico;
}

void Imagem::LerImagem()
{
	string nome_imagem = getNome_da_Imagem();
	ifstream  Arquivo_Imagem(nome_imagem.c_str());
        if (Arquivo_Imagem.is_open())
        {
        	cout <<"Arquivo aberto!!"<< endl;
        }
        else
        {
        	cout <<"ERRO , Nome do arquivo invalido" << endl;
        	exit(EXIT_FAILURE);
        }	
	cout << "Lendo Imagem...!!" << endl;
	string lixo,pixels_conteudo,N_magico,transforma;
	char pixel;
	char c;
	getline(Arquivo_Imagem,N_magico);
	setNumero_magico(N_magico);
	if (N_magico != "P6")
	{
		cout << "Imagem não é ppm!!" << endl;
		exit(EXIT_FAILURE);
	}
	getline(Arquivo_Imagem,lixo);
	int inteiro;
	if(lixo[0] != '#')
	{
		int num = stoi(lixo);
		setLargura(num);
	}
	else
	{
		setComentario(lixo);
		while(1){
			Arquivo_Imagem.get(c);
			if(c == '#'){
				getline(Arquivo_Imagem,lixo);
			}
			else{
				while(c != ' '){
					transforma += c;
					Arquivo_Imagem.get(c);
				}
				break;
			}
		}
		int num = stoi(transforma);
		setLargura(num);
	}

        Arquivo_Imagem >> inteiro;
        setAltura(inteiro);

        Arquivo_Imagem >> inteiro;
        setCor_Max(inteiro);

	getline(Arquivo_Imagem,pixels_conteudo);
	
	while (!Arquivo_Imagem.eof())
	{
		Arquivo_Imagem.get(pixel);
		pixels_conteudo += pixel;
	}	
	setPixels(pixels_conteudo);
	Arquivo_Imagem.close();
}

