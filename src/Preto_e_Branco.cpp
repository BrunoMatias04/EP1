#include "Preto_e_Branco.hpp"

Preto_e_Branco::Preto_e_Branco(){
}
Preto_e_Branco::~Preto_e_Branco(){
}
void Preto_e_Branco::Filtrar(){
	string nome_nova_imagem = "Arquivo_Novo.ppm";
	ofstream Arquivo_Novo(nome_nova_imagem.c_str());
	Arquivo_Novo << getNumero_magico() << endl;
	Arquivo_Novo << getLargura() << endl;
	Arquivo_Novo << getAltura() << endl;
	Arquivo_Novo << getCor_Max() << endl;
	int aux = 0;
	int tamanho = getLargura()*getAltura();
	Pixel *pxl = getP();
	Pixel *New_pxl = (Pixel*)malloc(tamanho * sizeof(Pixel));
	while(aux < tamanho)
	{
		int grayscale_value = ((0.299*pxl[aux].getR())+(0.587*pxl[aux].getG())+(0.144*pxl[aux].getB()));
		pxl[aux].setR(grayscale_value);
		pxl[aux].setG(grayscale_value);
		pxl[aux].setB(grayscale_value);
		New_pxl[aux] = pxl[aux];
		aux++;
	}
	aux = 0;
	while(aux < tamanho)
	{
		Arquivo_Novo << New_pxl[aux].getR();
		Arquivo_Novo << New_pxl[aux].getG();
		Arquivo_Novo << New_pxl[aux].getB();
		aux++;
	}
	cout << "Filtrado e criado o arquivo ""Arquivo_Novo.ppm"" com a imagem filtrada na pasta do programa!!"<< endl;
	Arquivo_Novo.close();
}
