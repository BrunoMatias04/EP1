#include "Pixel.hpp"

Pixel::Pixel(){
	setR(' ');
	setG(' ');
	setB(' ');
}
Pixel::Pixel(unsigned char r,unsigned char g,unsigned char b){
	setR(r);
	setG(g);
	setB(b);
}
unsigned char Pixel::getR(){
	return r;
}	
unsigned char Pixel::getG(){
	return g;
}
unsigned char Pixel::getB(){
	return b;
}
void Pixel::setR(unsigned char r){
	this->r = r;
}
void Pixel::setG(unsigned char g){
	this->g = g;
}
void Pixel::setB(unsigned char b){
	this->b = b;
}
