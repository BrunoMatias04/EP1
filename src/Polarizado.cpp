#include "Polarizado.hpp"

Polarizado::Polarizado(){
}
Polarizado::~Polarizado(){
}
void Polarizado::Filtrar(){
	string nome_nova_imagem = "Arquivo_Novo.ppm";
	ofstream Arquivo_Novo(nome_nova_imagem.c_str());
	Arquivo_Novo << getNumero_magico() << endl;
	Arquivo_Novo << getLargura() << endl;
	Arquivo_Novo << getAltura() << endl;
	Arquivo_Novo << getCor_Max() << endl;
	int aux = 0;
	int tamanho = getLargura()*getAltura();
	Pixel *pxl = getP();
 	unsigned char CorMax = getCor_Max();
	while(aux < tamanho)
	{
		if(pxl[aux].getR() < (CorMax/2)){
			pxl[aux].setR(0);
		}
		else{
			pxl[aux].setR(CorMax);
		}
		if(pxl[aux].getG() < (CorMax/2)){
			pxl[aux].setG(0);
		}
		else{
			pxl[aux].setG(CorMax);
		}
		if(pxl[aux].getB() < (CorMax/2)){
                        pxl[aux].setB(0);
		}
                else{
                        pxl[aux].setB(CorMax);
		}
		aux++;
	}
	aux = 0;
	while(aux < tamanho)
	{
		Arquivo_Novo << pxl[aux].getR();
		Arquivo_Novo << pxl[aux].getG();
		Arquivo_Novo << pxl[aux].getB();
		aux++;
	}
	cout << "Filtrado e criado o arquivo ""Arquivo_Novo.ppm"" com a imagem filtrada na pasta do programa!!"<< endl;
	Arquivo_Novo.close();
}
