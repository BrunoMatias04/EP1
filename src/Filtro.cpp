#include "Filtro.hpp"
Filtro::~Filtro(){
}
Filtro::Filtro(){
	setP(NULL);
}
Filtro::Filtro(Pixel *p){
	setP(p);
}
Pixel * Filtro::getP(){
        return p;
}
void Filtro::setP(Pixel *p)
{
	this->p = p;
}

void Filtro::InserindoPixels(string pixels){
	
	int tamanho = pixels.size();	
	Pixel *pxl = (Pixel*)malloc(tamanho);
	int aux1 = 0 ,aux2 = 0;
	while(aux1 < tamanho )
	{
		pxl[aux2].setR(pixels[aux1]);
		aux1++;
		pxl[aux2].setG(pixels[aux1]);
		aux1++;
		pxl[aux2].setB(pixels[aux1]);
		aux1++;
		aux2++;
	}
	setP(pxl);
}
void Filtro::Filtrar(){
	string nome_nova_imagem = "Arquivo_Novo.ppm";
	ofstream Arquivo_Novo(nome_nova_imagem.c_str());
	Arquivo_Novo << getNumero_magico() << endl;
	Arquivo_Novo << getLargura() << endl;
	Arquivo_Novo << getAltura() << endl;
	Arquivo_Novo << getCor_Max() << endl;
	int aux = 0;
	int tamanho = getLargura()*getAltura();
	Pixel *pxl = getP();
	Pixel *New_pxl = (Pixel*)malloc(tamanho * sizeof(Pixel));
	while(aux < tamanho)
	{
		pxl[aux].setR(getCor_Max() - pxl[aux].getR());
		pxl[aux].setG(getCor_Max() - pxl[aux].getG());
		pxl[aux].setB(getCor_Max() - pxl[aux].getB());
		New_pxl[aux] = pxl[aux];
		aux++;
	}
	aux = 0;
	while(aux < tamanho)
	{
		Arquivo_Novo << New_pxl[aux].getR();
		Arquivo_Novo << New_pxl[aux].getG();
		Arquivo_Novo << New_pxl[aux].getB();
		aux++;
	}
	cout << "Filtrado e criado o arquivo ""Arquivo_Novo.ppm"" com a imagem filtrada na pasta do programa!!"<< endl;
	Arquivo_Novo.close();
}

