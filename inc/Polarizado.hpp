#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include "Filtro.hpp"

using namespace std;

class Polarizado : public Filtro {
public:
	Polarizado();
	~Polarizado();
	void Filtrar();
};
#endif

