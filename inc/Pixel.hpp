#ifndef PIXEL_HPP
#define PIXEL_HPP
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
using namespace std;


class Pixel{

private:
	unsigned char r;
	unsigned char g;
	unsigned char b;
public:
	Pixel();
	Pixel(unsigned char r,unsigned char g,unsigned char b);
	unsigned char getR();
	unsigned char getG();
	unsigned char getB();	
	void setR(unsigned char r);
	void setG(unsigned char g);
	void setB(unsigned char b);
};
#endif
