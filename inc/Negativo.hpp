#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include "Filtro.hpp"

using namespace std;

class Negativo : public Filtro{
public:
	Negativo();
	~Negativo();
	void Filtrar(); 
};
#endif

