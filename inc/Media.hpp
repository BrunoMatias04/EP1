#ifndef MEDIA_HPP
#define MEDIA_HPP

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include "Filtro.hpp"

using namespace std;

class Media : public Filtro {
public:
	Media();
	~Media();
	void Filtrar();
	void Filtrar5x5();
	void Filtrar7x7();
};
#endif

