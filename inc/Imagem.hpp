#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>

using namespace std;

class Imagem{
private:
	string Pixels;
	string Comentario;
	string Numero_magico;
	int Largura;
	int Altura;
	int Cor_Max;
	string Nome_da_Imagem;
public:
	Imagem();
	Imagem(string Pixels,string Comentario,string Numero_magico,int Largura,int Altura,int Cor_Max,string Nome_da_imagem);
	~Imagem();
	string	getPixels();
	string	getComentario();
	string	getNumero_magico();
	int	getLargura();
	int	getAltura();
	int	getCor_Max();
	string	getNome_da_Imagem();
	
	void	setPixels(string Pixels);
	void	setComentario(string Comentario);
	void    setNumero_magico(string Numero_magico);
	void	setLargura(int Largura);
	void	setAltura(int Altura);
	void	setCor_Max(int Cor_Max);
	void	setNome_da_Imagem(string Nome_da_imagem);
	void	LerImagem();
	
};
#endif
