#ifndef FILTRO_HPP
#define FILTRO_HPP

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Imagem.hpp"
#include "Pixel.hpp"
using namespace std;

class Filtro : public Imagem {
	private:
	Pixel *p;
	public:
	Filtro();
	Filtro(Pixel *p);
	~Filtro();
	Pixel * getP();
	void setP(Pixel *p);
	void InserindoPixels(string pixels);
	void Filtrar();
};
#endif

