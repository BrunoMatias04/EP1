#ifndef PRETO_E_BRANCO_HPP
#define PRETO_E_BRANCO_HPP

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include "Filtro.hpp"

using namespace std;

class Preto_e_Branco : public Filtro {
public:
	Preto_e_Branco();
	~Preto_e_Branco();
 	void Filtrar();
};
#endif

